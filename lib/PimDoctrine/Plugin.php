<?php
/**
 * Plugin Main
 */
namespace PimDoctrine;

use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;
use Gedmo\DoctrineExtensions;
use Pimcore\API\Plugin as PluginLib;
use Pimcore\Config;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\EventManager;
use Doctrine\ORM\Events;
use PimDoctrine\Extension\TablePrefix;

class Plugin extends PluginLib\AbstractPlugin implements PluginLib\PluginInterface
{
    /**
     * @return bool|string
     */
    public static function install()
    {
        $path = self::getInstallPath();

        if (!is_dir($path)) {
            mkdir($path);
        }

        if (!file_exists(static::cliConfigFilePath())) {
            file_put_contents(
                static::cliConfigFilePath(), file_get_contents(
                    static::pluginPath() . '/data/config/cli-config.php.dist'
                )
            );
        }

        if (!is_dir($path = PIMCORE_WEBSITE_VAR . '/plugins/pimdoctrine')) {
            mkdir($path);
            $file = $path . '/pimdoctrine-config.php';
            if (!file_exists($file)) {
                file_put_contents(
                    $file, file_get_contents(
                        static::pluginPath()
                        . '/data/config/pimdoctrine-config.php.dist'
                    )
                );
            }
        }

        if (self::isInstalled()) {
            return "PimDoctrine Plugin successfully installed.";
        } else {
            return "PimDoctrine Plugin could not be installed";
        }
    }

    /**
     * @return string
     */
    public static function getInstallPath()
    {
        return static::pluginPath() . '/data/installed';
    }

    /**
     * Get Plugin Path
     *
     * @return string
     */
    public static function pluginPath()
    {
        return PIMCORE_PLUGINS_PATH . '/PimDoctrine';
    }

    /**
     * @return string
     */
    public static function cliConfigFilePath()
    {
        return PIMCORE_DOCUMENT_ROOT . '/cli-config.php';
    }

    /**
     * @return bool
     */
    public static function isInstalled()
    {
        return is_dir(static::getInstallPath());
    }

    /**
     * @return string
     */
    public static function uninstall()
    {
        rmdir(self::getInstallPath());

        if (!self::isInstalled()) {
            return "PimDoctrine Plugin successfully uninstalled.";
        } else {
            return "PimDoctrine Plugin could not be uninstalled";
        }
    }

    /**
     * Init hook setup
     */
    public function init()
    {
        $this->_loadOnce();
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public static function getEntityManager()
    {
        if (!\Zend_Registry::isRegistered('doctrine.em')) {
            throw new \Exception('Entity manager not registered');
        }

        /** @var \Doctrine\ORM\EntityManager $em */
        $em = \Zend_Registry::get('doctrine.em');
        if (defined("PHPUNIT_TEST") && !$em->isOpen()) {
            //static::resetEntityManager();
        }
        return $em;
    }

    /**
     * Reset Entity Manager
     *
     * @param EntityManager $em
     * @return EntityManager
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public static function resetEntityManager(EntityManager $em = null)
    {
        if (null === $em) {
            $em = static::getEntityManager();
        }

        if (!$em->isOpen()) {
            $connection = $em->getConnection();
            $config = $em->getConfiguration();

            $em = $em->create(
                $connection, $config
            );
        }
        return $em;
    }

    protected function _loadOnce()
    {
        if (\Zend_Registry::isRegistered('doctrine.em')) {
            return false;
        } else {
            $pluginConfig = $this->_loadConfiguration();
            $doctrineConfig = $pluginConfig['doctrine'];

            //Entity Paths
            $paths = ['website/models/Website/Entity/'];
            if (is_array($doctrineConfig['paths'])) {
                $paths = $doctrineConfig['paths'];
            }
        }

        \Doctrine\Common\Annotations\AnnotationRegistry::registerFile(
            PIMCORE_DOCUMENT_ROOT . '/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/DoctrineAnnotations.php'
        );

        $cache = new \Doctrine\Common\Cache\ArrayCache;

        $annotationReader = new \Doctrine\Common\Annotations\AnnotationReader;
        $cachedAnnotationReader = new \Doctrine\Common\Annotations\CachedReader(
            $annotationReader, // use reader
            $cache // and a cache driver
        );

        $driverChain = new \Doctrine\ORM\Mapping\Driver\DriverChain();

        \Gedmo\DoctrineExtensions::registerAbstractMappingIntoDriverChainORM(
            $driverChain,
            $cachedAnnotationReader
        );

        $annotationDriver = new \Doctrine\ORM\Mapping\Driver\AnnotationDriver(
            $cachedAnnotationReader,
            $paths
        );

        //Doctrine Config
        foreach ((array)$doctrineConfig['namespaces'] as $namespace => $v) {
            $driverChain->addDriver($annotationDriver, $namespace);
        }


        $config = new \Doctrine\ORM\Configuration;

        //Proxy Generate
        $path = PIMCORE_WEBSITE_VAR . '/plugins/pimdoctrine';
        $config->setProxyDir($path);
        $config->setProxyNamespace('Proxy');
        $config->setAutoGenerateProxyClasses(false);

        $config->setMetadataDriverImpl($driverChain);

        $config->setMetadataCacheImpl($cache);
        $config->setQueryCacheImpl($cache);


        $evm = new \Doctrine\Common\EventManager();
        if (!empty($doctrineConfig['table_prefix'])) {
            $tablePrefix = new TablePrefix($doctrineConfig['table_prefix']);
            $evm->addEventListener(Events::loadClassMetadata, $tablePrefix);
        }

        $sluggableListener = new \Gedmo\Sluggable\SluggableListener;
        $sluggableListener->setAnnotationReader($cachedAnnotationReader);
        $evm->addEventSubscriber($sluggableListener);


        $treeListener = new \Gedmo\Tree\TreeListener;
        $treeListener->setAnnotationReader($cachedAnnotationReader);
        $evm->addEventSubscriber($treeListener);


        $loggableListener = new \Gedmo\Loggable\LoggableListener;
        $loggableListener->setAnnotationReader($cachedAnnotationReader);
        $evm->addEventSubscriber($loggableListener);

        $timestampableListener = new \Gedmo\Timestampable\TimestampableListener;
        $timestampableListener->setAnnotationReader($cachedAnnotationReader);
        $evm->addEventSubscriber($timestampableListener);

        $translatableListener = new \Gedmo\Translatable\TranslatableListener;
        $translatableListener->setTranslatableLocale('en');
        $translatableListener->setDefaultLocale('en');
        $translatableListener->setAnnotationReader($cachedAnnotationReader);
        $evm->addEventSubscriber($translatableListener);

        $sortableListener = new \Gedmo\Sortable\SortableListener;
        $sortableListener->setAnnotationReader($cachedAnnotationReader);
        $evm->addEventSubscriber($sortableListener);


        //$evm->addEventSubscriber(new \Doctrine\DBAL\Event\Listeners\MysqlSessionInit());

        //Connection Settings
        if (!empty($doctrineConfig['connection'])) {
            $dbParams = $doctrineConfig['connection']['orm_default'];
        } else {
            $systemConfig = Config::getSystemConfig()->toArray();
            $params = $systemConfig['database']['params'];

            // the connection configuration
            $dbParams = [
                'driver' => strtolower($systemConfig['database']['adapter']),
                'user' => $params['username'],
                'password' => $params['password'],
                'dbname' => $params['dbname'],
            ];
        }
        $em = \Doctrine\ORM\EntityManager::create($dbParams, $config, $evm);
        $em->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');

        //Enable debug output
        if (\Pimcore::inDebugMode()) {
            //$em->getConnection()->getConfiguration()->setSQLLogger(new EchoSQLLogger());
        }
        \Zend_Registry::set('doctrine.em', $em);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    protected function _loadDefaults()
    {
        if (!\Zend_Registry::isRegistered('doctrine.em')) {
            $pluginConfig = $this->_loadConfiguration();
            $doctrineConfig = $pluginConfig['doctrine'];

            //Connection Settings
            if (!empty($doctrineConfig['connection'])) {
                $dbParams = $doctrineConfig['connection']['orm_default'];
            } else {
                $systemConfig = Config::getSystemConfig()->toArray();
                $params = $systemConfig['database']['params'];

                // the connection configuration
                $dbParams = [
                    'driver' => strtolower($systemConfig['database']['adapter']),
                    'user' => $params['username'],
                    'password' => $params['password'],
                    'dbname' => $params['dbname'],
                ];
            }

            //Table Prefix
            $evm = new EventManager;
            if (!empty($doctrineConfig['table_prefix'])) {
                $tablePrefix = new TablePrefix($doctrineConfig['table_prefix']);
                $evm->addEventListener(Events::loadClassMetadata, $tablePrefix);
            }

            //Entity Paths
            $paths = ['website/models/Website/Entity/'];
            if (is_array($doctrineConfig['paths'])) {
                $paths = $doctrineConfig['paths'];
            }

            //$setup = Setup::createAnnotationMetadataConfiguration(
            //$paths, $isDevMode, null, null, false
            //);

            //Create Configuration File
            $setup = Setup::createConfiguration(\Pimcore::inDebugMode(), null, null);

            //Setup the annotation driver
            $annotationDriver = $setup->newDefaultAnnotationDriver($paths, false);

            //Driver Chain
            $driverChain = new MappingDriverChain();
            if (isset($doctrineConfig['gedmo'])) {
                DoctrineExtensions::registerAbstractMappingIntoDriverChainORM(
                    $driverChain,
                    $annotationDriver->getReader()
                );
            }

            //Doctrine Config
            foreach ((array)$doctrineConfig['namespaces'] as $namespace => $v) {
                $driverChain->addDriver($annotationDriver, $namespace);
            }

            //Metadata driver Implementation
            $setup->setMetadataDriverImpl($driverChain);

            //Gedmo
            if (isset($doctrineConfig['gedmo'])) {
                $this->setupGedmo(
                    $doctrineConfig['gedmo'],
                    $annotationDriver->getReader(),
                    $evm
                );
            }

            //Entity Manager
            $em = EntityManager::create(
                $dbParams, $setup, $evm
            );

            $em->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
            \Zend_Registry::set('doctrine.em', $em);
        }
    }

    /**
     * Gedmo
     *
     * @param array $config
     * @param CachedReader $cachedAnnotationReader
     * @param EventManager $evm
     */
    public function setupGedmo(
        array $config,
        CachedReader $cachedAnnotationReader,
        EventManager $evm
    ) {
        if ($config['SluggableListener']) {
            $sluggableListener = new \Gedmo\Sluggable\SluggableListener();
            $sluggableListener->setAnnotationReader($cachedAnnotationReader);
            $evm->addEventSubscriber($sluggableListener);
        }

        if ($config['TreeListener']) {
            $treeListener = new \Gedmo\Tree\TreeListener();
            $treeListener->setAnnotationReader($cachedAnnotationReader);
            $evm->addEventSubscriber($treeListener);
        }

        if ($config['TimestampableListener']) {
            $timestampableListener = new \Gedmo\Timestampable\TimestampableListener();
            $timestampableListener->setAnnotationReader($cachedAnnotationReader);
            $evm->addEventSubscriber($timestampableListener);
        }

        if ($config['SortableListener']) {
            $sortableListener = new \Gedmo\Sortable\SortableListener;
            $sortableListener->setAnnotationReader($cachedAnnotationReader);
            $evm->addEventSubscriber($sortableListener);
        }

        if ($config['IpTraceable']) {
            $ipTraceable = new \Gedmo\IpTraceable\IpTraceableListener();
            $ipTraceable->setAnnotationReader($cachedAnnotationReader);
            $evm->addEventSubscriber($ipTraceable);
        }
    }

    /**
     * @throws \Exception
     */
    protected function _loadConfiguration()
    {
        $file
            =
            PIMCORE_WEBSITE_VAR . '/plugins/pimdoctrine/pimdoctrine-config.php';
        if (!file_exists($file)) {
            throw new \Exception('pimdoctrine config file not found');
        }
        return include $file;
    }
}